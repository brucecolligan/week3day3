package com.techelevator;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class IteratorDemo {

	public static void main(String[] args) {
		
		Set<String> fruit = new HashSet<String>();
		fruit.add("Red Delicious Apple");
		fruit.add("Mango");
		fruit.add("Pineapple");
		fruit.add("Pear");
		fruit.add("Banana");
		fruit.add("Kiwi");

		Iterator<String> it = fruit.iterator();
		while(it.hasNext()) {
			String s = it.next();
			System.out.println(s);
		}
		
		it = fruit.iterator();
		while(it.hasNext()) {
			String s = it.next();
			if(s.startsWith("P")) {
				it.remove();
			}
		}
		
		System.out.println("\n\nAfter removing fruits whose name starts with 'P'\n");
		for(String s : fruit) {
			System.out.println(s);
		}
	}

}
