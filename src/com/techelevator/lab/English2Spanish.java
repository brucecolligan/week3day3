package com.techelevator.lab;

import java.util.HashMap;
import java.util.Map;

public class English2Spanish {
	
	public static void main(String[] args) {
		
		String english = args[0].toLowerCase();
		
		Map<String, String> eng2esp = new HashMap<String, String>();
		eng2esp.put("one", "Uno");
		eng2esp.put("two", "Dos");
		eng2esp.put("three", "Tres");
		eng2esp.put("four", "Cuatro");
		
		String spanish = eng2esp.get(english);
		
		if(spanish != null) {
			System.out.println(spanish);
		} else {
			System.out.println("I don't know that word :(");
		}
	}
}
